package Model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Array {
    private static int[] arr1 = new int[20];
    private static int[] arr2 = new int[20];

    //generate numbers for array
    private static void generateNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 20);
        }
    }
    //--------------------------------------------------
    //fill arrays
    static  {
        generateNumbers(arr1);
        generateNumbers(arr2);
    }

    //---------------------------------------------------
    //print arrays
    public void printArr() {
        System.out.println("First array:");
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }
        System.out.println();
        System.out.println("---------------");
        System.out.println("Second array:");
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + " ");
        }
    }

    //-----------------------------------------------------
    //create New Array With Same Numbers
    public void createNewArrWithSameNumbers() {
        Set<Integer> countSameNumb = new HashSet<>();
        for (int i = 0; i < arr1.length; i++) {
            int currNum = arr1[i];
            for (int j = 0; j < arr2.length; j++) {
                if (currNum == arr2[j]) {
                    countSameNumb.add(arr2[j]);
                }
            }
        }
        System.out.println();
        System.out.println("---------------");
        System.out.println("Third array with the same numbers:");
        int[] arr3 = new int[countSameNumb.size()];
        int i = 0;
        for (Integer integer : countSameNumb) {
            arr3[i] = integer;
            System.out.print(arr3[i] + " ");
            i++;
        }
    }

    //-----------------------------------------------------
    //create new array with different numbers
    public void createNewArrWithDifferentNumbers() {
        Set<Integer> set = new HashSet<>();
        int diffNum = 0;
        for (int i = 0; i < arr1.length; i++) {
            int currNum = arr1[i];
            for (int j = 0; j < arr2.length; j++) {
                if (currNum != arr2[j]) {
                    diffNum = arr2[j];
                }else if (currNum == arr2[j]) {
                    diffNum = 0;
                    break;
                }
            }
            if (currNum != 0) {
                set.add(currNum);
            }
        }
        System.out.println();
        System.out.println("---------------");
        System.out.println("Fourth array with the different numbers:");
//        int[] arr4 = new int[list.size()];
//        int j = 0;
//        for (int i : arr4) {
//            arr4[j] = i;
//            System.out.print(arr4[i] + " ");
//            j++;
//        }
        for (Integer integer : set) {
            System.out.print(integer + " ");
        }
    }
}



