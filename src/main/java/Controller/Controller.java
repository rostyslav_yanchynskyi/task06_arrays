package Controller;

public interface Controller {

    void printArr();
    void createNewArrWithSameNumbers();
    void createNewArrWithDifferentNumbers();
}
