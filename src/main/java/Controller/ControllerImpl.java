package Controller;

import Model.Array;

public class ControllerImpl implements Controller {
    Array array = new Array();
    @Override
    public void printArr() {
        array.printArr();
    }

    @Override
    public void createNewArrWithSameNumbers() {
        array.createNewArrWithSameNumbers();
    }

    @Override
    public void createNewArrWithDifferentNumbers() {
        array.createNewArrWithDifferentNumbers();
    }
}
