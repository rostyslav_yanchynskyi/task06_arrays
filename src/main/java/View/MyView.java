package View;

import Controller.*;
import Model.Array;

import java.util.Scanner;

public class MyView {
static Controller controller;

    public static void show() {
        controller = new ControllerImpl();
        //primitive user interface
        while (true) {
            System.out.println("1.Print 2 default arrays and third array with the same numbers." +
                    "\n" + "2.Print 2 default arrays and third array with different numbers.");

            Scanner scr = new Scanner(System.in);
            int num = scr.nextInt();

            if (num == 1) {
                controller.printArr();
                controller.createNewArrWithSameNumbers();
                break;
            }

            if (num == 2) {
                controller.printArr();
                controller.createNewArrWithDifferentNumbers();
                break;
            }
        }
    }
}
